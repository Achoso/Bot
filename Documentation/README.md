# LaTeX Dokumentation
Die Dokumentation des gesamten Projektes wird in LaTeX geschrieben.
Für die jeweiligen Module sind .tex Files vorgesehen in denen diese jeweils dokumentiert werden.
Zudem enthält das Verzeichnis Basics allgemeine Dokumentations-Teile, die nicht Modulspezifisch ist.

Um die Dokumentation zu kompilieren wird pdflatex verwendet.
Pdflatex ist in Miktex enthalten und kann über https://miktex.org/download heruntergeladen werden.

Nach der Installation kann das PDF über die Kommandozeile erstellt werden.
Für das kompilieren der .tex Dateien steht ein Batch-Skript zur Verfügung.
Unter Windows wird dazu CMD oder eine Powershell in diesem Verzeichnis geöffnet und dann das PDF mit folgendem Befehl erstellt:

	./compile
    
Sollten Nicht-Windows-User das Skript nicht ausführen können, kann auch folgendes im Terminal aufgerufen werden:

    pdflatex -synctex=1 -interaction=nonstopmode -aux-directory=tmp --src-specials document.tex
	
Beim ersten kompilieren müssen ggf. einige packages installiert werden. 
Miktex wird dann automatisch darauf hinweisen und das direkte Installieren der Packages ermöglichen.
Desweiteren werden Änderungen die das Inhaltsverzeichnis verändern erst nach dem zweiten mal kompilieren sichtbar.

Ein hilfreicher PDF-Viewer ist SumatraPDF, da dieser es ermöglicht die 
kompilierten Änderungen auch bei geöffnetem PDF direkt zu sehen.
Downloaden kann man ihn hier: https://www.sumatrapdfreader.org/free-pdf-reader.html