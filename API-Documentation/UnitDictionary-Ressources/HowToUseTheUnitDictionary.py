# -*- coding: utf-8 -*-
"""
Created on Sat Nov 18 18:20:32 2017

@author: JHMLap
"""
import re
import pickle

with open("UnitDictionary(Pickled)","rb") as f:
    unitdic = pickle.load(f)
    
## um Namen abzurufen    
nydusCanal = unitdic["NydusCanal"]
#oder die pysc2ID
pysc2Nr = unitdic["NydusCanal"]["sc2nr"]
nydusCanal = unitdic[pysc2Nr]

#um euch unbekannte key abzurufen:
pattern = re.compile(r'^Nydus')
matches = [x for x in unitdic.keys() if pattern.match(str(x))]
print(matches)
if matches:
    print(unitdic[matches[0]])
    print()
    ## eigentschaften:
    if "costresource" in unitdic[matches[0]]:
        print (unitdic[matches[0]]["costresource"])
        print()

# um die fürs erste relevanten daten schnell zu bekommen:
pattern = re.compile(r'^easy')
matches = [x for x in unitdic['SCV'].keys() if pattern.match(str(x))]
print(matches) ## DPS is damage per second