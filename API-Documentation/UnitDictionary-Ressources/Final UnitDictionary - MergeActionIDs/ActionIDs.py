# Extract Action IDs from TextFile and add them into the dictionary

# The following command can be run to show all availabel actions in the pysc2 API.
# python -m pysc2.bin.valid_actions
# Opening the file with all actions.
fp = open('List-of-valid-Actions.txt', 'r')
lines = fp.readlines()
fp.close()

# Delete all spaces
lines = [line.replace(' ', '') for line in lines]

# Generate a new file without unnecessary information
fp = open('ActionsFreeSpaces.txt', 'w')
for line in lines:
    newLine = '/'.join(line.split('/')[1:])
    newLine = newLine.split('(', 1)[0]
    newLine = newLine + '\n'
    fp.write(newLine)
fp.close()

# Open the generated file (just for checking purpose)
fp = open('ActionsFreeSpaces.txt', 'r')
lines = fp.readlines()
fp.close()

# Display some examples
for line in lines:
    check = ['Build', 'Barracks']
    if 'Build' in line and 'Barracks' in line and not 'Reactor' in line and not 'TechLab' in line:
        print(line)
