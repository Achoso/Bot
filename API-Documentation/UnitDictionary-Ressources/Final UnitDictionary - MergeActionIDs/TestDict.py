import pickle
#Open the dictionary
with open("UnitDictionary","rb") as f:
    unitdic = pickle.load(f)
f.close()

# Search for something and print it
for x in unitdic.values():
    if 'upgrade_Reactor_ID' in x: # Get al listings that contain the upgrade id for a Reactor
        print(x)
        print()
