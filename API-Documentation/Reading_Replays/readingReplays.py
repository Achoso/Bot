#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 10:47:29 2017

@author: root
"""

import pickle
with open('D:/pysc2daten/extractedSC2Replays/2aa82e5551bb426d9af6271df6638289Terran2','rb') as f:
    replaydat = pickle.load(f)
observations = replaydat[0]




class parseDing:
    def __init__(self):
        self.name = ""
        self.data = {}
        self.parent = self





anfang = parseDing()

deadunits = {}

class Unit:
    tags = {}
    
    def __init__(self, tag):
        self.tag = tag
        Unit.tags[tag] = self
        self.died = 0
        
    def update(self, attributes, step):
        self.attributes = attributes
        self.step = step
        
    def died(self, step):
        self.died = step
    

klammertiefe = 0

updateround = 0

step = 0
for obs1 in observations:
    ##obs1 = observations[0]    
    step = step +1
    
    obtext = str(obs1)
    units = obtext.split("units {")
    mapsplit = units[-1].split("map_state {")

    
    obs1.observation.player_common
    obs1.observation.raw_data.player.camera
    obs1.observation.raw_data.units   #display_type ist erstes attribut
    
    unitzeug = str(obs1.observation.raw_data.units)
    
    units = unitzeug.split(", ")

    for unit in units:
        unittemp = unit
        attributedic = {}
        unittemp = unit.replace("orders {","")
        unittemp = unittemp.replace("pos {","")
        unittemp = unittemp.replace("}","")
        attributes = unittemp.split("\n")
        for attribute in attributes:
            ab = attribute.split(": ")
            try:
                attributedic[ab[0].strip()] = ab[1].strip()
            except:
                pass #print(ab[0].strip())
        if (attributedic['tag']) not in Unit.tags.keys():
            a = Unit(attributedic['tag'].strip())
        Unit.tags[attributedic['tag']].update(attributedic, step)
    dellist = []
    for unit in Unit.tags.values():
        if unit.step < step:
            dellist.append(unit.tag)
    for dead in dellist:
        deadunits[dead.strip()] = [Unit.tags[dead.strip()],step]
        del Unit.tags[dead.strip()]


    