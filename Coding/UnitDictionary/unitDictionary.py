# The UnitDictionary and how it is created is described in the API Documentaion directory.

# Imports
import pickle
import os

from pysc2.lib import actions   # Necessary to get int action id by string

# Constants
PATH = os.path.abspath(os.path.dirname(__file__))
FILENAME = "UnitDictionary"

# Vars
dictionary = None

# TODO: For Schleifen bei Abfrage über ID durch Zugriff über ID als Key ersetzen

# Get the whole set of values by name
def getUnitDataByName(name):
	return dictionary[name]

# Get the whole set of values by id
def getUnitDataById(id):
	try:
		return dictionary[id]
	except KeyError as e:
		print('UNIT_DICTIONARY: KeyError: Bad id or the id is no integer')
		return None

# Get the name by the sc2 unit ID
def getUnitNameByUnitId(id):
	try:
		return dictionary[id]['id']
	except KeyError as e:
		print('UNIT_DICTIONARY: KeyError: Bad id or the id is no integer')
		return None

# Get the sc2 unit ID by name
def getUnitIdByUnitName(name):
	unit = dictionary[name]
	if 'sc2nr' in unit:
		return unit['sc2nr']
	else:
		return None

# Returns the ObjectType of the given id
# Possible Types: Unit, Projectile, Destructible, Resource, Other, Structure, Shape, Item
def getObjectTypeById(id):
	unit = getUnitDataById(id);
	if unit is not None and 'editorcategories' in unit:
		categoriesString = unit['editorcategories']
		categories = categoriesString.split(',')
		for category in categories:
			words = category.split(':')
			if words[0] == 'ObjectType':
				return words[1]
	else:
		return None

# Returns the [Minerals, Vespene] needed for building / producing the Object
def getResourcesById(id):
	unit = getUnitDataById(id)
	resources = [0, 0]

	if unit is not None and 'costresource' in unit:
		costresources = unit['costresource']
		if 'Minerals' in costresources:
			resources[0] = int(costresources['Minerals'])
		if 'Vespene' in costresources:
			resources[1] = int(costresources['Vespene'])

		return resources
	else:
		return None

# Get the id of the structure that produces the unit
def getProducingStructureIdByUnitId(id):
	name = getUnitNameByUnitId(id)
	for unit in dictionary.values():
		if 'techtreeproducedunitarray' in unit and name in unit['techtreeproducedunitarray']:
			return unit['sc2nr']
	return None

# Get the race of a unit by name
def getRaceByName(name):
	return(dictionary[name]['race'])

# Get the race of a unit by id
def getRaceById(id):
	return(getRaceByName(getUnitNameByUnitId(id)))

# Returns the ActionID that is needed to perform a ActionCall to pysc2
def getBuildActionIdByName(name):
	return( extractActionId(dictionary[name]['buildID']) )

#
def getBuildActionIdById(id):
	return( getBuildActionIdByName(getUnitNameByUnitId(id)) )

# Returns the Action Id wichich performs the train action
def getTrainActionIdByName(name):
	return( extractActionId(dictionary[name]['trainID']) )

#
def getTrainActionIdById(id):
	return( getTrainActionIdByName(getUnitNameByUnitId(id)) )

# Returns the ActioID to Upgrade a Building
def getUpgradeActionIdbyId(id, upgradeId):
	if upgradeId == 6: # Reactor sc2ID
		return( extractActionId( dictionary[getUnitNameByUnitId(id)]['upgrade_Reactor_ID']) )
	if upgradeId == 5: # Techlab sc2ID
		return( extractActionId( dictionary[getUnitNameByUnitId(id)]['upgrade_Techlab_ID']) )
	return( None )

def getFoodByName(name):
	if 'food' in dictionary[name]:
		return(-1 * int(dictionary[name]['food']))
	else:
		return 0

def getFoodById(id):
	return(getFoodByName(getUnitNameByUnitId(id)))

# Function to get the Action Id as int
def extractActionId(actionIdStr):
    return (actions.FUNCTIONS[actionIdStr].id)

# Function to initialize the unit dictionary
def initModule():
    global dictionary
    if dictionary == None:
        dictionary = readDataset(PATH, FILENAME)

# Functions
def readDataset(path, filename):
	with open(os.path.join(PATH , FILENAME), "rb") as file:
		data = pickle.load(file)
		return data

# This Code is only executed once on the module import.
initModule()

# Test function
if __name__ == '__main__':

	# How to axes something in the unitdictionary (bad way)

	#for unit in dictionary.values():
	#	if 'sc2nr' in unit.keys() and (unit['sc2nr'] == id):
	#		return unit['id']

    '''
    print(getProducingStructureIdByUnitId(getUnitIdByUnitName('Marine')))
    print(getRaceById(73))
    '''
    print(getUnitIdByUnitName('TechLab'))
    print(getUnitDataByName('TechLab'))
    #print(getBuildActionIdByName('TechLab'))
