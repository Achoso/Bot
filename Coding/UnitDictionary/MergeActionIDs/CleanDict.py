import pickle
# Open old Dict
with open("UnitDictionary(Pickled)","rb") as f:
    unitdic = pickle.load(f)
f.close()

# edit Dict
nDict = dict(unitdic)
for x, y in unitdic.items():
    if "sc2nr" not in y:
        del nDict[x]
    elif "id" not in y:
        del nDict[x]

# Action IDs
fp = open('ActionsFreeSpaces.txt', 'r')
lines = fp.readlines()
lines = [line.replace('\n', '') for line in lines]
fp.close()
# Add new Values
for x, y in nDict.items():
    id = y['id']
    for line in lines:
        # buildID
        if 'Build' in line and not 'Reactor' in line and not 'TechLab' in line and id in line:
            nDict[x]['buildID'] = line
        # trainID
        if 'Train' in line and id in line:
            nDict[x]['trainID'] = line
        # Upgrades:
        # Reactor
        if 'Build' in line and 'screen' in line and 'Reactor' in line and not 'Barracks' in line and not 'Factory' in line and not 'Starport' in line and ( 'Barracks' in id or 'Factory' in id or 'Starport'in id ):
            nDict[x]['upgrade_Reactor_ID'] = line
        # TechLab
        if 'Build' in line and 'screen' in line and 'TechLab' in line and not 'Barracks' in line and not 'Factory' in line and not 'Starport' in line in line and ( 'Barracks' in id or  'Factory' in id or 'Starport'in id ):
            nDict[x]['upgrade_Techlab_ID'] = line
        # CommandCenter
        if 'OrbitalCommand' in line and 'Morph' in line and 'CommandCenter' in id:
            nDict[x]['upgrade_Base_ID'] = line
        # Hatchery
        if 'Hive' in line and 'Morph' in line and 'Hatchery' in id:
            nDict[x]['upgrade_Base_ID'] = line
        # Hive
        if 'Lair' in line and 'Morph' in line and 'Hive' in id:
            nDict[x]['upgrade_Base_ID'] = line

# Save new Dict
with open("UnitDictionary", "wb") as f:
    pickle.dump(nDict, f, protocol=pickle.HIGHEST_PROTOCOL)
f.close()

# Readble text
f = open('dataset.txt', 'w')

count = 0
for unit, value in unitdic.items():
    count += 1
    if "id" in value:
        print()
        f.write("\n")
        print(count)
        f.write(str(count))
        f.write("\n")
        print(value["id"])
        f.write(value["id"])
        f.write("\n")
        for x in value:
            line = str(x) + " " + str(value[x])
            print(line)
            f.write(line)
            f.write("\n")

f.close()
'''
with open("CleanDict","rb") as f:
    unitdic = pickle.load(f)
f.close()
nDict = dict(unitdic)
for x, y in unitdic.items():
    if "sc2nr" not in y:
        print (y)
    elif "id" not in y:
        print (y)
'''
