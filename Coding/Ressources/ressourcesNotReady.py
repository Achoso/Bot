from pysc2.lib import actions
from pysc2.lib import features
import pysc2.agents.UnitDictionary.unitDictionary as ud

#Functions
ID_SELECT_POINT = actions.FUNCTIONS.select_point.id
ID_INDEX_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
ID_SELECT_IDLE_WORKER = actions.FUNCTIONS.select_idle_worker.id

# Variables
ID_COMMANDCENTER = 18
ID_REFINERY = 20
ID_TERRAN_SCV = 45
ID_MINERALFIELD = 341
ID_MOVE_CAMERA = 1
ID_SELECT_UNIT = 5

# Parameters
SCREEN = [0]

IDX_MINERALS = 1
IDX_VESPENE = 2

class Ressources:
    # Initialize class specific variables
    def __init__(self, actionQueue, abstractionHub):
        self.actionQueue = actionQueue
        self.abstractionHub = abstractionHub
        self.reservedMinerals = 0
        self.reservedVespene = 0
        self.notMining = 0
        self.scvList = []
        self.mineralFieldList = []
        self.refineryList = []
        self.commandCenterLocation = []

    
    # Sends Worker to mine vespene
    def mineVespene(self, workerPosX, workerPosY, selected = False, obs):
        storeRefineryLocations()
        for refinery in self.refineryList:
            if refinery[2] < 4:
                choosenRefinery = refinery
        selected = selectIdleWorker(obs);
        target = (choosenRefinery[0], choosenRefinery[1])
        if selected = True:
            self.actionQueue.put(actions.FunctionCall(ID_SELECT_POINT, [SCREEN, target]))    # Correct Function Call needs to be added
        else:
            selectWorker(obs);
            self.actionQueue.put(actions.FunctionCall(ID_MOVE_CAMERA,))     # Correct Function Call needs to be added
            self.actionQueue.put(actions.FunctionCall(ID_SELECT_POINT, [SCREEN, target]))

    # Sends Idle Worker to mine Minerals
    def mineMinerals(self):
        storeRefineryLocations()
        for mineralField in self.mineralFieldList:
            if mineralField[2] < 4:
                choosenMineralField = mineralField
        target = (choosenRefinery[0], choosenRefinery[1])
        selected = selectIdleWorker(obs);
        if selected = True:
            self.actionQueue.put(actions.FunctionCall(ID_SELECT_POINT, [SCREEN, target]))
        else:
            selectWorker(obs);
            self.actionQueue.put(actions.FunctionCall(ID_MOVE_CAMERA,))
            self.actionQueue.put(actions.FunctionCall(ID_SELECT_POINT, [SCREEN, target])) # Correct position still not implemented


    # Creates a list of positions of all SCVs seen in the screen
    def updateScvList(self):
        if self.mineralField == []:
            units = self.abstractionHub.giveAllUnits()
            for i in range(0, len(units)):
                if units[i].type == ID_TERRAN_SCV:
                    self.scvList.append([ int(units[i].xPos), int(units[i].yPos), UNUSED ])

    # Returns the  X - and Y- position of an scv who is closesed at the mineral field
    def getWorkerCoordinates(self):
        self.updateScvList()
        self.notMining += 1
        scvCoordinates = self.scvList[0]

        for i in range(0, len(self.scvList)):
            for u in range(0, len(self.scvList[i])):
                if (self.scvList[i][u] - self.mineralField[i][u] < scvCoordinates[u] - self.mineralField[i][u] )
                    scvCoordinates = self.scvList[i]
        return scvCoordinates
        
    # Function to select an idle worker
    def selectIdleWorker(self, obs):
        if self.notMining > 0:
            self.notMining -= 1

        if SELECT_IDLE_WORKER in obs.observation["available_actions"]:
            #print("SELECT idle worker")
            self.actionQueue.put(actions.FunctionCall(SELECT_IDLE_WORKER, [SCREEN]))
            #self.actionQueue.put(actions.FunctionCall())
            return (True)
        else:
            #print("NOT SELECT idle worker")
            return (False)

    # Function to select a worker by position
    def selectWorker(self, obs):
        target = self.getWorkerCoordinates()
        self.actionQueue.put(actions.FunctionCall(ID_SELECT_POINT, [SCREEN, target]))

    # Returns available ressources
    def getRessources(self, obs):
        minerals = obs.observation["player"][IDX_MINERALS] - self.reservedMinerals
        vespene = obs.observation["player"][IDX_VESPENE] - self.reservedVespene
        ressources = [minerals, vespene]
        #print(ressources)
        return ressources

    # Checks if enough ressources are available for a unit
    def checkIfPossible(self, obs, unitId):    # Reservations need to be implemented!
        ressources = ud.getResourcesById(unitId)
        #print(ressources)
        if ((self.getRessources(obs)[0] - ressources[0]) >= 0) and ((self.getRessources(obs)[1] - ressources[1]) >= 0):
            #print("Enough ressources")
            return True
        else:
            #print("Not Enough Ressources")
            return False

    # reserves ressources for units to be build
    def reserveRessources(self, unitId):
        ressources = ud.getResourcesById(unitId)
        self.reservedMinerals += ressources[0]
        self.reservedVespene += ressources[1]

    # clears reserved ressources after unit ist build by another class
    def clearReservedRessources(self, unitId):
        ressources = ud.getResourcesById(unitId)
        self.reservedMinerals -= ressources[0]
        self.reservedVespene -= ressources[1]

    # Stores the (x, y) Coordinates for each refinery to mine vespene
    def storeRefineryLocations(self):
        if self.refineryList == []:
            units = self.abstractionHub.giveAllUnits()
            for i in range(0, len(units)):
                if units[i].type == ID_REFINERY:
                    self.refineryList.append([ int(units[i].xPos), int(units[i].yPos), 0 ])

    # Stores the (x, y) Coordinates for each mineral field
    def getMineralFieldLocations(self, obs):
        if self.mineralFieldList == []:
            units = self.abstractionHub.giveAllUnits()
            for i in range(0, len(units)):
                if units[i].type == ID_MINERALFIELD:
                    self.mineralFieldList.append([ int(units[i].xPos), int(units[i].yPos), 0 ])