# Idea: Units get send to clusters of minerals on the Minimap or enemy buildings without own Units nearby.
# selection of Scouts: SVCs or cheap mobile units
# protection of scouts: None as of now

from pysc2.lib import actions, features
import pysc2.agents.UnitDictionary.unitDictionary as ud
import pysc2.agents.Abstractions.AbstractionHub
import numpy as np
np.set_printoptions(threshold=np.inf)

SELECT_IDLE_WORKER = actions.FUNCTIONS.select_idle_worker.id
MOVE_TO_MINIMAP = actions.FUNCTIONS.Move_minimap.id

class scout:
	def __init__(self, actionQueue=None, abstractionHub=None, debugOutputEnabled=False):
		self.actionQueue = actionQueue                  # Queue from agent to add actions
		self.abstractionHub = abstractionHub            # AbstractionHub instance
		self.debugOutputEnabled = debugOutputEnabled    # Flag to enable or disable debug output
		self.dict = ud
		
	def _getScoutableMineralCluster(self):
		mineralblobs = np.transpose((self.abstractionHub.obsParser.Mplayer_relative == 3).nonzero()) #neutral Unit #Mvisibility
		# choose random for now
		return np.random.choice(mineralblobs)
	
	def _getAVolunteer(self):
		# select an idle worker

		if SELECT_IDLE_WORKER in obs.observation["available_actions"]:
			self.actionQueue.put(actions.FunctionCall(SELECT_IDLE_WORKER, [[0]]))
		else:
					print("SCOUTER: Give me Volunteers!")
		
	def scoutLocation(self):
		self._getAVolunteer()
		self.actionQueue.put(actions.FunctionCall(MOVE_TO_MINIMAP, [_getScoutableMineralCluster()]))