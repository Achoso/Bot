
from pynput import keyboard
from pysc2.agents.ConstructionBuilder.construction_builder import ConstructionBuilder, Building

import pysc2.agents.UnitDictionary.UnitDictionary as ud

BUILDER = None
OBS = None

def on_press(key):
    '''
    try:
        print('alphanumeric key {0} pressed'.format(key.char))
    except AttributeError:
        print('special key {0} pressed'.format(key))
    '''
    
def on_release(key):

    # Stop listener
    if key == keyboard.Key.esc:
        print("LISTENER: stopped")
        return False
    
    # Handle pressed keys
    else:
        try: k = key.char       # single-char keys
        except: k = key.name    # other keys
        
        if BUILDER == None and OBS == None:
            print("\nLISTENER: Builder and Obs are None!")
        else:
            if k == '1':
                print("\nLISTENER: Request SupplyDepot")
                unitId = ud.getUnitIdByUnitName("SupplyDepot")
                
                # TODO: Check resources                 -> RESSOURCES
                BUILDER.selectWorker(OBS)             # -> RESSOURCES / AGENT
                BUILDER.addNewBuilding(OBS, unitId)   # -> BUILD ORDER
                
            elif k == '2':
                print("\nLISTENER: Request Refinery")
                unitId = ud.getUnitIdByUnitName("Refinery")
                
                # TODO: Check resources                 -> RESSOURCES
                BUILDER.selectWorker(OBS)             # -> RESSOURCES / AGENT
                BUILDER.addNewBuilding(OBS, unitId)   # -> BUILD ORDER
                
            elif k == '3':
                print("\nLISTENER: Request Barracks")
                unitId = ud.getUnitIdByUnitName("Barracks")
                
                # TODO: Check resources                 -> RESSOURCES
                BUILDER.selectWorker(OBS)             # -> RESSOURCES / AGENT
                BUILDER.addNewBuilding(OBS, unitId)   # -> BUILD ORDER
                
            elif k == '4':
                print("\nLISTENER: Request TechLab")
                unitId = ud.getUnitIdByUnitName("TechLab")
                
                # TODO: Check resources                 -> RESSOURCES
                BUILDER.selectWorker(OBS)             # -> RESSOURCES / AGENT
                BUILDER.addNewBuilding(OBS, unitId)   # -> BUILD ORDER
                
            else:
                print("\nLISTENER: Unknown command.")
                
# Start key listener in new thread
def startKeyListener(builder=None):
    global BUILDER
    BUILDER = builder
    
    #ud.init()
    
    lis = keyboard.Listener(on_press=on_press, on_release=on_release)
    lis.start()
    
# Update obs
def updateObs(obs=None):
    global OBS
    OBS = obs

