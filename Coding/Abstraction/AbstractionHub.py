
import pysc2.agents.Abstraction.UnitController as UnitController
import pysc2.agents.Abstraction.MapController as MapController
#import ObsParser
import pysc2.agents.Abstraction.ObsParser as ObsParser
import pysc2.agents.Abstraction.Unit as Unit
import numpy as np
import copy

class AbstractionHub(object):

  """


  :version:
  :author:
  """
  def __init__(self):
    self.obsParser = None

  def getObservations(self, obs):
     if self.obsParser:
         self.obsParser.grabObs(obs)
     else:
         self.obsParser = ObsParser.ObsParser(obs)
         self.obsParser.grabObs(obs)
     [sureReadUnits,possibleReadUnits] = self.obsParser.getUnits()
     self.obsParser.accountForUnits(sureReadUnits,possibleReadUnits)

  def incorporateActionTaken(self, CommandNr, Parameter1, Parameter2, Parameter3, Parameter4):
    """
    marks a unit with a command as busy (like a training barack, or a building worker), Handles expectations

    @param int CommandNr :
    @param int Parameter1 :
    @param int Parameter2 :
    @param int Parameter3 :
    @param int Parameter4 :
    @return  :
    @author
    """
    pass

  def worldClickToMiniMapClick(self,XPos,YPos):
     return self.obsParser._W2MCrds(int(XPos),int(YPos))

  def worldPositionToScreenMoveAndScreenPosition(self,XPos,YPos):
     #returns a list of two tupels: the first element is the minimap-click in (x,y), the second is the screen click in (x,y)
     # i the first element is empty the unit is on the Screen. If the first element has coordinates it is best to move the screen and then
     # get the updated position for the unit to select before clicking on the screen (executing the second suggestion)
     if self.obsParser.ScreenStartX < XPos < self.obsParser.ScreenEndX and self.obsParser.ScreenStartY < YPos < self.obsParser.ScreenEndY:
         return [[],self.obsParser._W2SCrds(XPos,YPos)]
     else:
         return [[self.obsParser._W2MCrds(XPos,YPos)],(int(self.obsParser.ScreenXCells/2), int(self.obsParser.ScreenYCells/2))]

  def giveAllUnits(self):
     #gives all Units as a List of Unit Objects the Abstraction Layer thinks are in the game right now
     return copy.copy(self.obsParser.unitController.trackedUnits)

  def giveAllUnitsNearPoint(self,XPos,YPos,radius):
     #gives all Units as a List of Unit Objects in a radius (actually a square with Sides 2*radius) near a certain Point. The Units are ordered by distance (first unit is the nearest etc)
     return self.obsParser.unitController.findMatches(XPos, YPos,matches = 0, attributes = None, xrange = radius, yrange = radius, accountedUnits = True)

  def giveTerrain(self,XStart = 0, XEnd = -1, YStart = 0, YEnd = -1):
     #gives a numpy array the size of the current Worldmap (or as specified by the parameters) according to the unitradius (one unitradius = 1 px) in the dictionary with values corresponding to the observations
     # you can run an edge detector to find flat terrain or passageways ... maybe i will implement this later
     XStart = max(0,min(self.obsParser.mapController.terrainMap.shape[0]-1,XStart))
     YStart = max(0,min(self.obsParser.mapController.terrainMap.shape[1]-1,YStart))
     if XEnd > -1:
       XEnd = max(XEnd, XStart)
     if YEnd > -1:
       YEnd = max(YEnd, YStart)
     return self.obsParser.mapController.terrainMap[XStart:XEnd, YStart:YEnd]

  def getListByID(self, SC2id):
     return self.obsParser.unitController.findMatches(0, 0,matches = 0, attributes = {'type':SC2id, 'player':1}, xrange = 99999999, yrange = 99999999, accountedUnits = True)

  def S2WPos(self, ScreenX,ScreenY):
  # Takes a Position on the current Screen and translates this to World-Coordinates
      return self.obsParser.S2WCrds(ScreenX,ScreenY)
      
  def W2MPos(self, WorldX, WorldY):
  # Takes a world position and translates this to Minimap-Coordinates
      return self.obsParser._W2MCrds(WorldX,WorldY)
