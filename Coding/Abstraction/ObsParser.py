# -*- coding: utf-8 -*-
"""
Created on Thu Nov 30 14:32:17 2017

@author: JHMLap
"""

import os
import socket
import pickle
import numpy as np
from skimage.measure import label
#import UnitController
#import MapController
import pysc2.agents.Abstraction.UnitController as UnitController
import pysc2.agents.Abstraction.MapController as MapController
import pysc2.agents.UnitDictionary.unitDictionary as Unitdict
from scipy.misc import imresize

# TODO: Using the UnitDictionary from Module Dictionary

# Path and name of the dictionary
MOD_PATH = os.path.abspath(__file__)
PATH = os.path.dirname(MOD_PATH) + "\\"
FILENAME = "UnitDictionary(Pickled)"

class ObsPickler:  ## DEBUG Class for getting observations from starcraft
    def _write(self,conn, data = [0,[]]):
        f = conn.makefile('wb', 8192 )
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
        f.close()

    def _read(self,conn):
        f = conn.makefile('rb', 8192 )
        data = pickle.load(f)
        f.close()
        return data

    def _getObs(self,conn):
        conn.send(" ".encode())
        return self._read(conn)
    def __init__(self):
        host = socket.gethostname()
        port = 999
        self.conn = socket.socket()
        self.conn.connect(('localhost',port))
        self.conn.send(" ".encode())
        specs = self._read(self.conn)
        obslist = []
        obs = self._getObs(self.conn)
        obslist.append(obs)
        for i in range (15):
            self._write(self.conn)
            obs = self._getObs(self.conn)
            obslist.append(obs)
        with open("someObs","wb") as f:
            pickle.dump(obslist,f)

class ObsParser:
    def getObs(self):
        self.obsNr += 1
        return self.obslist[self.obsNr]

    def registerUnitController(self, unitController):
        self.unitController = unitController

    def __init__(self, obs = None):
        #with open("someObs", "rb") as f:
        #    self.obslist = pickle.load(f)
        self.verbosity = False
        self.obsNr = -1
        #obs = self.getObs()
        camera = obs[3]['minimap'][3]
        Sunit_type = obs[3]['screen'][6]
        self.player_id = obs[3]['player'][0]
        MiniMapToScreenX = np.max(camera,0)
        MiniMapToScreenY = np.max(camera,1)
        ScreenXPosPercent = (MiniMapToScreenX * np.linspace(1,len(MiniMapToScreenX),num=len(MiniMapToScreenX))).sum()/(MiniMapToScreenX.sum()*len(MiniMapToScreenX))
        ScreenYPosPercent = (MiniMapToScreenY * np.linspace(1,len(MiniMapToScreenY),num=len(MiniMapToScreenY))).sum()/(MiniMapToScreenY.sum()*len(MiniMapToScreenY))
        self.unitDic = Unitdict.dictionary
        self.screenToWorldRatioX = MiniMapToScreenX.sum()/len(MiniMapToScreenX)
        self.screenToWorldRatioY = MiniMapToScreenY.sum()/len(MiniMapToScreenY)

        mainBuildingsize = float(self.unitDic["Nexus"]["radius"])/2

        mainBuildings = ["Nexus","CommandCenter","Hatchery"]
        Races = ["Protoss", "Terran", "Zerg"]
        for i in range(len(mainBuildings)):
            cluster = (Sunit_type == self.unitDic[mainBuildings[i]]["sc2nr"]).nonzero()
            if len(cluster[0]) > 1:
                self.playerRace = Races[i]
                break

        xDia = cluster[0][-1] - cluster[0][0]
        yDia = max(cluster[1]) - min(cluster[1])

        self.xDiaToPixel = xDia/mainBuildingsize
        self.yDiaToPixel = yDia/mainBuildingsize

        self.ScreenXCells = Sunit_type.shape[0]/mainBuildingsize
        self.ScreenYCells = Sunit_type.shape[1]/mainBuildingsize

        self.WorldXCells = int(self.ScreenXCells / self.screenToWorldRatioX)
        self.WorldYCells = int(self.ScreenYCells / self.screenToWorldRatioY)

        self.unitController = UnitController.UnitController()
        self.unitController.registerObsParser(self)

        self.mapController = MapController.MapController(self.WorldXCells, self.WorldYCells, camera.shape[0], camera.shape[1])
        self.mapController.registerUnitController(self.unitController)
        self.unitController.registerMapController(self.mapController)
        self.mapController.registerObsParser(self)
        self.mapController._initWorld(obs[3]['minimap'][0])

    def ScreenPos(self):
        self.ScreenStartX, self.ScreenStartY = self.S2WCrds(0,0)
        self.ScreenEndX, self.ScreenEndY = self.S2WCrds(self.Screep.shape[0]-1,self.Screep.shape[1]-1)
        if self.verbosity:
             print("new Screen Position: "+str(self.ScreenStartX)+", "+str(self.ScreenStartY))

    def S2WCrds(self, ScreenX, ScreenY): # SceenToWorldCoordinates
        MiniMapToScreenX = np.max(self.camera,0)
        MiniMapToScreenY = np.max(self.camera,1)
        ScreenXPosPercent = (MiniMapToScreenX * np.linspace(1,len(MiniMapToScreenX),num=len(MiniMapToScreenX))).sum()/(MiniMapToScreenX.sum()*len(MiniMapToScreenX))
        ScreenYPosPercent = (MiniMapToScreenY * np.linspace(1,len(MiniMapToScreenY),num=len(MiniMapToScreenY))).sum()/(MiniMapToScreenY.sum()*len(MiniMapToScreenY))
        if self.verbosity:
           print("ScreenMM% = "+str(ScreenXPosPercent)+", "+str(ScreenYPosPercent))
        ScreenMidPointinWorldX = ScreenXPosPercent * self.WorldXCells #WorldX
        ScreenMidPointinWorldY = ScreenYPosPercent * self.WorldYCells #WorldY   i want to count from bottomleft
        ScreenXMidPoint = self.ScreenXCells/2 #C
        ScreenYMidPoint = self.ScreenYCells/2 #C
        WorldXPoint = ScreenX+ScreenMidPointinWorldX-ScreenXMidPoint
        WorldYPoint = ScreenY+ScreenMidPointinWorldY-ScreenYMidPoint
        return (WorldXPoint,WorldYPoint)

    def M2WCrds(self,MapX, MapY): #MiniMapToWorldCoordinates
        WorldXPoint = MapX/self.camera.shape[0] * self.WorldXCells #WorldX
        WorldYPoint = MapY/self.camera.shape[1] * self.WorldYCells #WorldY   i want to count from bottomleft
        return (WorldXPoint,WorldYPoint)

    def _W2SCrds(self, WorldX, WorldY): # SceenToWorldCoordinates
        MiniMapToScreenX = np.max(self.camera,0)
        MiniMapToScreenY = np.max(self.camera,1)
        ScreenXPosPercent = (MiniMapToScreenX * np.linspace(1,len(MiniMapToScreenX),num=len(MiniMapToScreenX))).sum()/(MiniMapToScreenX.sum()*len(MiniMapToScreenX))
        ScreenYPosPercent = (MiniMapToScreenY * np.linspace(1,len(MiniMapToScreenY),num=len(MiniMapToScreenY))).sum()/(MiniMapToScreenY.sum()*len(MiniMapToScreenY))
        ScreenMidPointinWorldX = ScreenXPosPercent * self.WorldXCells #WorldX
        ScreenMidPointinWorldY = ScreenYPosPercent * self.WorldYCells #WorldY   i want to count from bottomleft
        ScreenXMidPoint = self.ScreenXCells/2 #C
        ScreenYMidPoint = self.ScreenYCells/2 #C
        ScreenXPoint = int(WorldX-ScreenMidPointinWorldX+ ScreenXMidPoint)
        ScreenYPoint = int(WorldY-ScreenMidPointinWorldY +ScreenYMidPoint)
        return (ScreenXPoint,ScreenYPoint)

    def _W2MCrds(self,WorldX, WorldY): #MiniMapToWorldCoordinates
        MapXPoint = (WorldX)/self.WorldXCells *self.camera.shape[0] #WorldX
        MapYPoint = (WorldY)/self.WorldYCells *self.camera.shape[1] #WorldY   i want to count from bottomleft
        return (int(MapXPoint),int(MapYPoint))

    def _centerpointsOfUnit(self, Points, unitRadius):
        ## gets an uniform array
        centerpoints = []
        Pointsl = label(Points)
        for cl in range(1,np.max(Pointsl)+1):
            #startpoint:
            clusterpoints = (Pointsl == cl).nonzero()[0]
            start = clusterpoints[0]

            end = clusterpoints[-1]
            amountUnits = np.maximum(int(round((end-start)/unitRadius+0.5,0)),1)
            newradius = (end-start)/amountUnits
            for ctr in range(0,amountUnits):
                centerpoints.append(start + newradius/2 + newradius * ctr )
        return centerpoints

    def grabObs(self, obs):
        #obs = self.getObs()

        #Minimap
        self.Mheightmap = obs[3]['minimap'][0]
        self.Mvisibility = obs[3]['minimap'][1]
        self.Mcreep = obs[3]['minimap'][2]
        self.camera = obs[3]['minimap'][3]
        self.Mplayer_id = obs[3]['minimap'][4]
        self.Mplayer_relative = obs[3]['minimap'][5]
        self.Mselected = obs[3]['minimap'][6]

        #Screen
        self.Sheightmap = obs[3]['screen'][0]
        self.Svisibility = obs[3]['screen'][1]
        self.Screep = obs[3]['screen'][2]
        self.Spower = obs[3]['screen'][3]
        self.Splayer_id = obs[3]['screen'][4]
        self.Splayer_relative = obs[3]['screen'][5]
        self.Sunit_type = obs[3]['screen'][6]
        self.Sselected = obs[3]['screen'][7]
        self.Sunit_hit_points = obs[3]['screen'][8]
        self.Sunit_hit_points_ratio = obs[3]['screen'][9]
        self.Sunit_energy = obs[3]['screen'][10]
        self.Sunit_energy_ratio = obs[3]['screen'][11]
        self.Sunit_shields = obs[3]['screen'][12]
        self.Sunit_shields_ratio = obs[3]['screen'][13]
        self.Sunit_density = obs[3]['screen'][14]
        self.Sunit_density_aa = obs[3]['screen'][15]
        self.Seffects = obs[3]['screen'][16]

        #Score
        self.score = obs[3]['score_cumulative'][0]
        self.idle_production_time = obs[3]['score_cumulative'][1]
        self.idle_worker_time = obs[3]['score_cumulative'][2]
        self.total_value_units = obs[3]['score_cumulative'][3]
        self.total_value_structures = obs[3]['score_cumulative'][4]
        self.killed_value_units = obs[3]['score_cumulative'][5]
        self.killed_value_structures = obs[3]['score_cumulative'][6]
        self.collected_minerals = obs[3]['score_cumulative'][7]
        self.collected_vespene = obs[3]['score_cumulative'][8]
        self.collection_rate_minerals = obs[3]['score_cumulative'][9]
        self.collection_rate_vespene = obs[3]['score_cumulative'][10]
        self.spent_minerals = obs[3]['score_cumulative'][11]
        self.spent_vespene = obs[3]['score_cumulative'][12]

        #Player
        self.player_id = obs[3]['player'][0]
        self.minerals = obs[3]['player'][1]
        self.vespene = obs[3]['player'][2]
        self.food_used = obs[3]['player'][3]
        self.food_cap = obs[3]['player'][4]
        self.food_army = obs[3]['player'][5]
        self.food_workers = obs[3]['player'][6]
        self.idle_worker_count = obs[3]['player'][7]
        self.army_count = obs[3]['player'][8]
        self.warp_gate_count = obs[3]['player'][9]
        self.larva_count = obs[3]['player'][10]

        #Loop
        self.gameloop = obs[3]['game_loop'][0]

        self.ScreenPos()
        if self.verbosity:
           print("Terrain now")
           print(imresize(self.mapController.terrainMap[int(self.ScreenStartY)-2:int( self.ScreenEndY+2),int(self.ScreenStartX)-2:int(self.ScreenEndX)+2], (16,16)))
           print()

        self.mapController.updateLookedAt(self.ScreenStartX, self.ScreenStartY,self.ScreenEndX, self.ScreenEndY, self.Sheightmap)
        if self.verbosity:
           print("Terrain updated")
           print(imresize(self.mapController.terrainMap[int(self.ScreenStartY)-2:int( self.ScreenEndY)+2,int(self.ScreenStartX)-2:int(self.ScreenEndX)+2], (16,16)))
           print()

    def getUnits(self):

        helperCluster = label(self.Sselected * 10**14 + self.Splayer_id * 10**12 + self.Sunit_hit_points * 10**8 + self.Sunit_shields * 10**4 + self.Sunit_type)
        overlaplist = (self.Sunit_density > 1).nonzero()
        sureReadUnits = []
        possibleReadUnits = []
        for cl in range(1,np.max(helperCluster)+1):
            #isolate the cluster
            clus = (helperCluster == cl)
            tclus = clus.nonzero()
            #get unit type and other data (for finding the exaxt expected unit)
            unittype = self.Sunit_type[tclus[0][0],tclus[1][0]]
            hitpoints = self.Sunit_hit_points[tclus[0][0],tclus[1][0]]
            shields = self.Sunit_shields[tclus[0][0],tclus[1][0]]
            unitPlayer = self.Splayer_id[tclus[0][0],tclus[1][0]]
            Uselect = self.Sselected[tclus[0][0],tclus[1][0]]
            player_rel = self.Splayer_relative[tclus[0][0],tclus[1][0]]
            #expand cluster with the overlaps

            tclus2 = np.array([np.hstack((tclus[0],overlaplist[0])),np.hstack((tclus[1],overlaplist[1]))])
            #build uniform map of expanded cluster and cluster again to only get overlaps connected to the original cluster
            shape = helperCluster.shape
            tclus3 = np.zeros(shape)
            tclus3[tclus2[0],tclus2[1]] = 1
            tclus3 = label(tclus3)
            #select the expanded cluster
            clus = (tclus3 == tclus3[tclus[0][0],tclus[1][0]])
            tclus = clus.nonzero()
            try:
              unitSizeY = float(self.unitDic[unittype]["radius"]) * 3*self.yDiaToPixel
              unitSizeX = float(self.unitDic[unittype]["radius"]) * 3*self.xDiaToPixel
            except:
              unitSizeY = 8
              unitSizeX = 8
              # print("ObsParser: check radius of "+str(unittype))

            unitsInCluster = 0
            # start getting the centerpoints of that cluster
            while (True):
                unitsInCluster += 1
                # do we have possible units left?
                if len(tclus[0]) == 0:
                    break
                #try eliminating the cluster with a single Unit
                if max(tclus[0])-min(tclus[0]) <= unitSizeX and max(tclus[1]) - min(tclus[1]) <= unitSizeY:
                    ymeans = (max(tclus[1]) + min(tclus[1]))/2
                    xmeans = (max(tclus[0]) + min(tclus[0]))/2
                else:

                    #find first leftmost slice of the cluster:
                    leftslice = clus[tclus[0][0],:]

                    if sum(leftslice) == 0: # some safety, should not get triggered
                        break
                    if unitsInCluster > 25:
                        if self.verbosity: print("insanity! too many units in one parseCluster")
                        break

                    #check if size is smaller than upper unitSize
                    if sum(leftslice) <= round(unitSizeY+0.5,0):
                        #usual case: leftmost slice is one unit - we can get ymean:
                        ymeans = (leftslice.nonzero()[0][0] + leftslice.nonzero()[0][-1])/2

                    else:
                        #we have multiple units above each other - we need to slice from the top on this path to separate them
                        ymeans = self._centerpointsOfUnit(leftslice, unitSizeY)[0]

                    #now get the x means of the first found y:
                    xmeans = tclus[0][0] + int(unitSizeX/2) - 1

                    #only the first x and y slice will be the center, so we only take that as a unit

                #get affected cells:
                xrange = list(range(int(xmeans-unitSizeX/2),int(xmeans+unitSizeX/2)+1))
                yrange = list(range(int(ymeans-unitSizeY/2),int(ymeans+unitSizeY/2)+1))
                #if unit sticks out of the screen get the unit inside to avoid errors
                xrange = np.maximum(np.minimum(xrange,clus.shape[0]-1),0)
                yrange = np.maximum(np.minimum(yrange,clus.shape[1]-1),0)
                # get confidence for this prediction (Unit actually visible?)
                overlap = np.sum([xrange[0] < ptr[0] < xrange[-1] and yrange[0] < ptr[1] < yrange[-1] for ptr in zip(overlaplist[0],overlaplist[1])])
                if overlap/(unitSizeY*unitSizeX) == 1:
                    confidence = 0
                elif overlap/(unitSizeY*unitSizeX) > 0.3:
                    confidence = 0.5
                else:
                    confidence = 1
                #delete found units from clus
                clus[xrange[0]:xrange[-1] , yrange[0]:yrange[-1]] = False
                #delete units from tclus
                ntclus = [[],[]]
                for i in range(len(tclus[0])):
                    if xrange[-1] >= tclus[0][i] >= xrange[0] and yrange[-1] >= tclus[1][i] >= yrange[0]:
                        continue
                    else:
                        ntclus[0].append(tclus[0][i])
                        ntclus[1].append(tclus[1][i])
                tclus =( np.array(ntclus[0]),np.array(ntclus[1])  )
                #register previously found unit
                if unitsInCluster == 1:
                    sureReadUnits.append({"player": unitPlayer, "type": unittype, "hp": hitpoints, "sh": shields, "selected" : Uselect, "x":xmeans, "y":ymeans, "confidence":confidence, "player_relative" : player_rel })
                else:
                    try:
                        if xmeans == possibleReadUnits[-1]["x"] and ymeans == possibleReadUnits[-1]["y"] and unittype == possibleReadUnits[-1]["type"]:
                            break
                    except:
                        pass ##first possible unit
                    possibleReadUnits.append({"player": unitPlayer, "type": unittype, "hp": hitpoints, "sh": shields, "selected" : Uselect, "x":xmeans, "y":ymeans, "confidence":confidence, "player_relative" : player_rel })
        return [sureReadUnits,possibleReadUnits]


    def accountForUnits(self, sureSeenUnits, possibleUnits):
        self.unitController.updateUnits()
        for unit in sureSeenUnits:

            wX,wY = self.S2WCrds(unit['y'],unit['x'])
            if self.verbosity == True and unit['type'] == 18:
               print("ScreenFound: "+str(unit['x'])+", "+str(unit['y'])+ " ScreenStart: "+str(self.ScreenStartX)+", "+str(self.ScreenStartY)+ " ScreenEnd: "+str(self.ScreenEndX)+", "+str(self.ScreenEndY)+" World: "+str(wX)+", "+str(wY))
               print("Screen%: "+str(unit['x']/self.ScreenXCells)+", "+str(unit['y']/self.ScreenYCells)+" World%: "+str(wX/self.WorldXCells)+", "+str(wY/self.WorldYCells))
            unit['x'] = wX
            unit['y'] = wY
            match = self.unitController.findMatches(wX,wY,matches = 1, attributes = {'type':unit['type'], 'player':unit['player']}, xrange = self.ScreenXCells, yrange = self.ScreenYCells) #, 'selected':unit['selected']
            if match:
                match[0].xPos = wX
                match[0].yPos = wY
                self.unitController.accountFor(match[0])
            else:
                self.unitController.newSightOrWayOff.append(unit)
        for unit in possibleUnits:
            wX,wY = self.S2WCrds(unit['x'],unit['y'])
            unit['x'] = wX
            unit['y'] = wY
            match = self.unitController.findMatches(wX,wY,matches = 1, attributes = {'type':unit['type'], 'player':unit['player']}, xrange = self.ScreenXCells, yrange = self.ScreenYCells) #, 'selected':unit['selected']
            if match:
                match[0].xPos = wX
                match[0].yPos = wY
                self.unitController.accountFor(match[0])
            else:
                self.unitController.maybeNewSightOrWayOff.append(unit)

        # now match everything  off screen
        for blob in np.transpose(((self.Mselected * (self.camera==0)) == 1).nonzero()):
            wX,wY = self.M2WCrds(blob[0],blob[1])
            matches = self.unitController.findMatches(wX, wY,matches = 0, attributes = None, xrange = 1* self.WorldXCells/self.camera.shape[0], yrange = 1* self.WorldYCells/self.camera.shape[1])
            matchNotfound = True
            for match in matches:
                if match.attributes['selected'] == '1':
                    self.unitController.accountFor(match)
                    #matchNotfound = False
                if match.attributes['player'] == str(self.Mplayer_id[blob[0],blob[1]]): ## and same unselected units should be accounted for
                    self.unitController.accountFor(match)
                elif match.attributes['player_relative'] == '2' or match.attributes['player_relative'] == '1': ## also friendly and neutral units which may have less priority
                    self.unitController.accountFor(match)
            if matchNotfound:
                self.unitController.unaccountedBlob(wX,wY,blob, {'selected':1})

        #now for pure self blobs
        for blob in np.transpose(((self.Mplayer_id == self.player_id) * (self.camera==0)).nonzero()):
            wX,wY = self.M2WCrds(blob[0],blob[1])
            matches = self.unitController.findMatches(wX, wY,matches = 0, attributes = None, xrange = 1* self.WorldXCells/self.camera.shape[0], yrange = 1* self.WorldYCells/self.camera.shape[1])
            matchNotfound = True
            if matches:
                for match in matches:
                    if str(match.attributes['player']) == str(self.Mplayer_id[blob[0],blob[1]]):
                        self.unitController.accountFor(match)
                        matchNotfound = False
                    elif str(match.attributes['player_relative']) == '3' or str(match.attributes['player_relative']) == '1':
                        self.unitController.accountFor(match)
            if matchNotfound:
                self.unitController.unaccountedBlob(wX,wY,blob, {'player':self.Mplayer_id[blob[0],blob[1]]})

        #enemys
        for blob in np.transpose(((self.Mplayer_relative == 2) * (self.camera == 0)).nonzero()):
            wX,wY = self.M2WCrds(blob[0],blob[1])
            matches = self.unitController.findMatches(wX, wY,matches = 0, attributes = None, xrange = 0.5* self.WorldXCells/self.camera.shape[0], yrange = 0.5* self.WorldYCells/self.camera.shape[1])
            matchNotfound = True
            for match in matches:
                if str(match.attributes['player_relative']) == str(self.Mplayer_relative[blob[0],blob[1]]):
                    self.unitController.accountFor(match)
                    matchNotfound = False
                elif str(match.attributes['player_relative']) == '3' or str(match.attributes['player_relative']) == '1':
                    self.unitController.accountFor(match)
            if matchNotfound:
                self.unitController.unaccountedBlob(wX,wY,blob, {'player':self.Mplayer_id[blob[0],blob[1]]})

        #allys
        for blob in np.transpose(((self.Mplayer_relative == 1) * (self.camera == 0)).nonzero()):
            wX,wY = self.M2WCrds(blob[0],blob[1])
            matches = self.unitController.findMatches( wX, wY,matches = 0, attributes = None, xrange = 0.5* self.WorldXCells/self.camera.shape[0], yrange = 0.5* self.WorldYCells/self.camera.shape[1])
            matchNotfound = True
            for match in matches:
                if str(match.attributes['player_relative']) == str(self.Mplayer_relative[blob[0],blob[1]]):
                    self.unitController.accountFor(match)
                    matchNotfound = False
                elif str(match.attributes['player_relative']) == '3':
                    self.unitController.accountFor(match)
            if matchNotfound:
                self.unitController.unaccountedBlob(wX,wY,blob, {'player':self.Mplayer_id[blob[0],blob[1]]})

        #neutral
        for blob in np.transpose(((self.Mplayer_relative == 3) * (self.camera == 0 )).nonzero()):
            wX,wY = self.M2WCrds(blob[0],blob[1])
            matches = self.unitController.findMatches( wX, wY, matches = 0,attributes = None, xrange = 0.5* self.WorldXCells/self.camera.shape[0], yrange = 0.5* self.WorldYCells/self.camera.shape[1])
            matchNotfound = True
            for match in matches:
                if str(match.attributes['player_relative']) == str(self.Mplayer_relative[blob[0],blob[1]]):
                    self.unitController.accountFor(match)
                    matchNotfound = False
            if matchNotfound:
                self.unitController.unaccountedBlob(wX,wY,blob, {'player':self.Mplayer_id[blob[0],blob[1]]})
        self.unitController.blobsAdd(np.transpose(((self.Mplayer_id > 0)*(self.camera == 0)).nonzero()))
        self.unitController.resolveMismatches()


if __name__ == "main":
	o = ObsParser()
	for z in range(10):
	    o.grabObs()
	    '''print("Screenstart :"+ str(np.transpose((o.camera == 1).nonzero())[0]))
	    print(o.ScreenStartX)
	    print(o.ScreenStartY)
	    main = np.transpose((o.Sunit_type == 59).nonzero())
	    print(o.S2WCrds(main[0][0],main[0][1]))
	    print()'''

	    [sureReadUnits,possibleReadUnits] = o.getUnits()

	    o.accountForUnits(sureReadUnits,possibleReadUnits)
	    print()
	    print("round "+str(z)+":")
	    for unit in o.unitController.trackedUnits:
	        print(str(unit.type) + " at "+str(unit.xPos) + " "+str(unit.yPos))
