# -*- coding: utf-8 -*-
"""
Created on Sat Dec 16 12:46:58 2017

@author: JHMLap
"""
#import ObsParser
#import AbstractionHub
#import Unit
#import UnitController
#import MapController
import pysc2.agents.Abstraction.ObsParser
import pysc2.agents.Abstraction.AbstractionHub as AbstractionHub
import pysc2.agents.Abstraction.Unit
import pysc2.agents.Abstraction.UnitController
import pysc2.agents.Abstraction.MapController
import pickle
import pysc2.lib.features as ft
from scipy.misc import imresize
import numpy as np

#This class loads a replay gives parsed and original layer data for the obsparser

class ReplayProvider:
	def __init__(self):
		pass
	
	def loadReplay(self, replaypath):
		with open(replaypath,'rb') as f:
			self.replaydat = pickle.load(f)
			self.observations = self.replaydat[0]

	def parseReplay(self, fromStep, toStep):
		klammertiefe = 0
		feat = ft.Features(game_info = self.replaydat[4])
		updateround = 0
		parsedObs = []
		rawObs = []
		deadunits = {}
		step = 0
		for obs1 in self.observations:
		    ##obs1 = observations[0]    
		    step = step +1
		    if step < fromStep:
		      continue
		    obtext = str(obs1)
		    units = obtext.split("units {")
		    mapsplit = units[-1].split("map_state {")
		    units[-1] = units[-1].split("map_state {")[0]
		
		    
		    obs1.observation.player_common
		    obs1.observation.raw_data.player.camera
		    obs1.observation.raw_data.units   #display_type ist erstes attribut
		    
		    unitzeug = str(obs1.observation.raw_data.units)
		    
		    units = unitzeug.split(", ")
		    unitsThisStep = []
		    for unit in units:
		        unittemp = unit
		        attributedic = {}
		        unittemp = unit.replace("orders {","")
		        unittemp = unittemp.replace("pos {","")
		        unittemp = unittemp.replace("}","")
		        attributes = unittemp.split("\n")
		        for attribute in attributes:
		            ab = attribute.split(": ")
		            try:
		                attributedic[ab[0].strip()] = ab[1].strip()
		            except:
		                pass #print(ab[0].strip())
		        if (attributedic['tag']) not in ReplayUnit.tags.keys():
		            a = ReplayUnit(attributedic['tag'].strip())
		        ReplayUnit.tags[attributedic['tag']].update(attributedic, step)
		        unitsThisStep.append(attributedic)
		    dellist = []
		    for unit in ReplayUnit.tags.values():
		        if unit.step < step:
		            dellist.append(unit.tag)
		    for dead in dellist:
		        deadunits[dead.strip()] = [ReplayUnit.tags[dead.strip()],step]
		        del ReplayUnit.tags[dead.strip()]
		    parsedObs.append(unitsThisStep)
		    rawObs.append(feat.transform_obs(obs1.observation))
		
		    if step > toStep:
		       break
		self.parsedObs = parsedObs
		self.rawObs = rawObs
	
	def saveParsedThings(self, savePath):
		with open(savePath,'wb') as f:
			pickle.dump([self.rawObs,self.parsedObs],f)

class ParsedObsAndTruthComparer:
	def __init__(self):
		self.abstractionHub = AbstractionHub.AbstractionHub()
		self.step = 0
		self.oldScreenStartX = 0
		self.oldScreenStartY = 0
		
	def loadTestSet(self, filename):
		with open(filename,'rb') as f:
			self.ObsList,self.groundTruthList = pickle.load(f)
		
	def compare(self):
		obs = []
		for i in range(3):
			 obs.append([])
		obs.append(self.ObsList[self.step])
		self.abstractionHub.getObservations(obs)
		parsedData = self.abstractionHub.giveAllUnits()
		print("terrain + cam:")
		print(imresize(self.abstractionHub.obsParser.mapController.terrainMap, (16,16)) * (imresize(self.abstractionHub.obsParser.camera, (16,16)) < 5)+((imresize(self.abstractionHub.obsParser.camera, (16,16))>5)*888).astype(int))
		print()
		#print("Screen is belived to be here")
		#m = imresize(self.abstractionHub.obsParser.mapController.terrainMap, (16,16))
		#xstart =int(self.abstractionHub.obsParser.ScreenStartX*16/self.abstractionHub.obsParser.WorldXCells) 
		#xend = int(self.abstractionHub.obsParser.ScreenEndX*16/self.abstractionHub.obsParser.WorldXCells)
		#ystart = int(self.abstractionHub.obsParser.ScreenStartY*16/self.abstractionHub.obsParser.WorldYCells)
		#yend = int(self.abstractionHub.obsParser.ScreenEndY*16/self.abstractionHub.obsParser.WorldYCells)
		#m[ ystart: yend ,xstart:xend ] = 0
		#print(m)
		#print()
		print("terrain screen:")
		print(imresize(self.abstractionHub.obsParser.Sheightmap, (16, 16)))
		print()
		#print("Screen Command")
		#print(imresize((self.abstractionHub.obsParser.Sunit_type == 18), (16, 16)))
		#print()

		tagMap = {}
		for unit in parsedData:
			if unit.type == 18:
				print("found: "+str(unit.xPos)+","+str(unit.yPos))
		truthData = self.groundTruthList[self.step]
		for unit in truthData:
			if unit['unit_type'] == '18':
				print("truth: "+str(unit['x'])+","+str(unit['y']))
		self.step += 1
		if self.oldScreenStartX != self.abstractionHub.obsParser.ScreenStartX or self.oldScreenStartY != self.abstractionHub.obsParser.ScreenStartY:
			print("Screen moved!")
		self.oldScreenStartX = self.abstractionHub.obsParser.ScreenStartX
		self.oldScreenStartY = self.abstractionHub.obsParser.ScreenStartY


class ReplayUnit:
    tags = {}
    
    def __init__(self, tag):
        self.tag = tag
        ReplayUnit.tags[tag] = self
        self.died = 0
        
        
    def update(self, attributes, step):
        self.attributes = attributes
        self.step = step
        
    def died(self, step):
        self.died = step
    


if __name__ == "__main__":
	#d = ReplayProvider()
	#d.loadReplay(r'C:\Users\JHMLap\Desktop\Uni\advancedSimulation\extractedSC2Replays\b92c4387855b49e7b997aa545101e45aTerran1')
	#d.parseReplay(0,1150)
	#d.saveParsedThings('ReplayToDebugAbstraction')
	c = ParsedObsAndTruthComparer()
	c.loadTestSet('ReplayToDebugAbstraction')
	counter = 0
	for i in range(1000):
		print()
		print("round: "+str(counter))
		counter +=1
		c.compare()