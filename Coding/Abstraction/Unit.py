from pysc2.agents.Abstraction.MapController import *
import uuid
import numpy as np

class Unit(object):
  activeUnits = set()
  deadUnits = set()
  mapController = None
  unitController = None
 
  def __init__(self, x, y, typ, faction, attributes):
      self.type = typ
      self.lastCorrection = 0
      self.xPos = x
      self.yPos = y
      self.oldXPos = x
      self.oldYPos = y
      self.faction = faction
      self.attributes = attributes
      self.listAttributes = None
      self.tag = uuid.uuid4()
      self.busy = 0
      self.job = ""
      self.jobtarget = ""
      self.direction = -1
      self.speed = 0
      self.inCargoOf = 0
      self.accounted = False
      self.existsIn = 0
      self.killMe = False
      self._register()

  def registerController(mapController, unitController): #called when mapcontroller registers at unitcontroller
	  Unit.mapController = mapController
	  Unit.unitController = unitController

  def _register(self):
    """
    @param MapController MapController : 
    @return  :
    @author
    """
    if not self.killMe:
      for i in Unit.mapController.returnSegments(self.xPos, self.yPos):
        i[0].add(self)
      Unit.unitController.newTrackedUnits.append(self)

  def update(self):
    """
    @return  :
    @author
    """
    self.accounted = False
    if not self.killMe:
       try:
          self.speed = max(0,min(4, np.sqrt((self.oldYPos-self.yPos)**2+(self.oldXPos-self.xPos**2))))
          self.direction = np.arctan2(self.oldYPos-self.yPos,self.oldXPos-self.xPos)
       except:
          self.direction = -1000
       pass
       #if self.direction >= -999:
#	        self.yPos = max(min(self.yPos + np.sin(self.direction) * self.speed,Unit.mapController.MapY),0)
#	        self.xPos = max(min(self.xPos + np.cos(self.direction) * self.speed,Unit.mapController.MapX),0)
       self._register()
       self.oldYPos = self.yPos
       self.oldXPos = self.xPos
       
		
  def strike(self):
	  self.killMe = True

  def __JobFinished(self):
    """
     

    @return  :
    @author
    """
    pass

  def WOCorrection(self,wX,wY):
	  self.xPos = wX
	  self.yPos = wY

