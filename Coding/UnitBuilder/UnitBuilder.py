from pysc2.lib import actions
from pysc2.lib import features
from pysc2.agents.ConstructionBuilder.construction_builder import Building
import pysc2.agents.UnitDictionary.unitDictionary as ud

# Functions
_SELECT_POINT = actions.FUNCTIONS.select_point.id
_RALLY_UNITS_SCREEN = actions.FUNCTIONS.Rally_Units_screen.id
_RALLY_UNITS_MINIMAP = actions.FUNCTIONS.Rally_Units_minimap.id
_MOVE_CAMERA = actions.FUNCTIONS.move_camera.id

# Features
_UNIT_TYPE = features.SCREEN_FEATURES.unit_type.index
_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index

# Parameters
_PLAYER_SELF = 1
_SCREEN = [0]
_SUPPLY_USED = 3
_SUPPLY_MAX = 4
_QUEUED = [1]
_MAX_TRIES_TO_FIND_BUILDING = 20

class UnitBuilder:

    # Possible states of the UnitBuilder
    STATE_SELECT_BUILDING = 0
    STATE_SET_RALLY_POINT = 1
    STATE_BUILD_UNIT = 2
    STATE_FINISHED = 3

    # Function to initialize the UnitBuilder.
    def __init__(self, actionQueue=None, abstractionHub=None, debugOutputEnabled=False):
        self.actionQueue = actionQueue                          # Queue from agent to add actions
        self.abstractionHub = abstractionHub                    # AbstractionHub instance
        self.debugOutputEnabled = debugOutputEnabled            # Flag to enable or disable debug output
        self.unitFactoryId = -1                                 # Id of the building, where the unit can be built
        self.lastUnitFactoryId = -1                             # Saves the Id of the last building, in which we built
        self.unitFactoryBuildingIdx = 0                         # Index to go through the building list
        self.currentState = UnitBuilder.STATE_SELECT_BUILDING   # Current state of the UnitBuilder
        self.base_top_left = None                               # Position of our base (Top-Left or Bottom-Right)
        self.noBuildingFoundCount = 0                           # Increases, if no available building was found
        self.unitCount = 0                                      # Number of Units to be built

    # Function to set the rally point
    def setRallyPoint(self, moveCoords, clickCoords):
        self.actionQueue.put( actions.FunctionCall(_MOVE_CAMERA, [ [moveCoords[0][0], moveCoords[0][1]] ]) )
        self.actionQueue.put( actions.FunctionCall(_RALLY_UNITS_SCREEN, [_SCREEN, [clickCoords[0], clickCoords[1]]]) )

    # Function to return the current state of the UnitBuilder
    def setCurrentState(self, currentState):
        self.currentState = currentState

    # Function to set the current state of the UnitBuilder
    def getCurrentState(self):
        return self.currentState

    # Function to build units
    def setUnits(self, unitID, count, obs, constructionBuilder):
        # Check if all units were built. Then it is a new task.
        if self.unitCount == 0:
            self.unitCount = count

        # Set the Id of the building, where the unit can be built
        unitFactoryId = ud.getProducingStructureIdByUnitId(unitID)

        # Get a list of buildings with the unitFactoryId
        unitFactoryBuildings = constructionBuilder.getListOfBuildingsById(unitFactoryId)

        # Choose building from list of buildings to produce units and add action to select the building
        if self.currentState == UnitBuilder.STATE_SELECT_BUILDING:
            if self.debugOutputEnabled:
                print('UNITBUILDER: STATE_SELECT_BUILDING')

            self.currentState = UnitBuilder.STATE_BUILD_UNIT
            
            # If it is the same building we built in last time, increase the index. Otherwise set it to 0
            if self.lastUnitFactoryId == unitFactoryId:
                self.unitFactoryBuildingIdx = self.unitFactoryBuildingIdx + 1

                if self.unitFactoryBuildingIdx >= len(unitFactoryBuildings):
                    self.unitFactoryBuildingIdx = 0
            else:
                self.unitFactoryBuildingIdx = 0

            # Get the building, where we want to build in
            unitFactoryBuilding = unitFactoryBuildings[self.unitFactoryBuildingIdx]
            
            self.lastUnitFactoryId = unitFactoryId
            
            # moveCoords represents the coordinates to click on the minimap
            # clickCoords represents the coordinates to click on the screen
            (moveCoords, clickCoords) = self.abstractionHub.worldPositionToScreenMoveAndScreenPosition(unitFactoryBuilding.getWorldX(), unitFactoryBuilding.getWorldY())

            # If the building is not on our screen, move the camera.
            if len(moveCoords) != 0:
                self.actionQueue.put( actions.FunctionCall(_MOVE_CAMERA, [ [moveCoords[0][0], moveCoords[0][1]] ]) )
                self.currentState = UnitBuilder.STATE_SELECT_BUILDING
            else: # Search on Screen for Building
                if self.debugOutputEnabled:
                    print('UNITBUILDER: Selecting building on screen.')
                self.actionQueue.put( actions.FunctionCall(_SELECT_POINT, [_SCREEN, [clickCoords[0], clickCoords[1]]]) )

            # If we have selected the building, we can go to the next state
            if self.currentState != UnitBuilder.STATE_SELECT_BUILDING:
                if not unitFactoryBuilding.isRallyPointSet:
                    self.currentState = UnitBuilder.STATE_SET_RALLY_POINT
                else:
                    self.currentState = UnitBuilder.STATE_BUILD_UNIT

        # Add action to produce units
        elif self.currentState == UnitBuilder.STATE_BUILD_UNIT:
            if self.debugOutputEnabled:
                print('UNITBUILDER: STATE_BUILD_UNIT', unitID)

            # Get the building, where we want to build in
            unitFactoryBuilding = unitFactoryBuildings[self.unitFactoryBuildingIdx]

            # Select other building if more than one exist and the current one is not finished yet or we are allready building units in this building
            if ((len(unitFactoryBuildings) > 1) and (unitFactoryBuilding.state != Building.STATE_FINISHED)) or (len(obs.observation["build_queue"]) > 1):
                self.currentState = UnitBuilder.STATE_SELECT_BUILDING
                self.noBuildingFoundCount = self.noBuildingFoundCount + 1
            # TODO Tell buildorder that something wasn't build.
            # Stop the building process if we tried to build units the amount of _MAX_TRIES_TO_FIND_BUILDING
            if self.noBuildingFoundCount >= _MAX_TRIES_TO_FIND_BUILDING:
                if self.debugOutputEnabled:
                    print('UNITBUILDER: Cant find valid building to produce Unit')
                self.currentState = UnitBuilder.STATE_FINISHED
                self.noBuildingFoundCount = 0

            # Add action to produce units if building is finished
            else:
                actionId = ud.getTrainActionIdById(unitID)
                if actionId in obs.observation["available_actions"]:
                    # Check for supply
                    if obs.observation["player"][_SUPPLY_USED] < obs.observation["player"][_SUPPLY_MAX]:
                        self.unitCount = self.unitCount - 1
                        self.actionQueue.put( actions.FunctionCall(actionId, [_QUEUED]) )

                    # Check if all units were built
                    if self.unitCount == 0:
                        self.currentState = UnitBuilder.STATE_FINISHED
                    else:
                        self.currentState = UnitBuilder.STATE_SELECT_BUILDING

        # Set the rally point
        elif self.currentState == UnitBuilder.STATE_SET_RALLY_POINT:
            if self.debugOutputEnabled:
                print('UNITBUILDER: STATE_SET_RALLY_POINT')

            # Get the position of our base
            if self.base_top_left is None:
                player_y, player_x = (obs.observation["minimap"][_PLAYER_RELATIVE] == _PLAYER_SELF).nonzero()
                self.base_top_left = player_y.mean() <= 31

            # Get the building, where we want to build in
            unitFactoryBuilding = unitFactoryBuildings[self.unitFactoryBuildingIdx]

            # Check that we don´t want to build a worker
            if unitID != 45 and unitID != 84 and unitID != 104:
                if self.base_top_left:
                    self.setRallyPoint([[29, 21], []], [42, 42])
                else:
                    self.setRallyPoint([[29, 46], []], [42, 42])

                unitFactoryBuilding.isRallyPointSet = True

            self.currentState = UnitBuilder.STATE_BUILD_UNIT
