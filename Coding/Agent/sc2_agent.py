
'''
To start this Agent use the following command:
    python -m pysc2.bin.agent --map Simple64 --agent pysc2.agents.Agent.sc2_agent.SC2Agent --agent_race T
'''

# PySC2 dependent imports
from pysc2.agents import base_agent
from pysc2.lib import actions, features

# Module imports
from pysc2.agents.ConstructionBuilder.construction_builder import ConstructionBuilder
from pysc2.agents.Abstraction.AbstractionHub import AbstractionHub
from pysc2.agents.UnitBuilder.UnitBuilder import UnitBuilder
from pysc2.agents.Ressources.ressources import Ressources
from pysc2.agents.WarHandler.warHandler import WarHandler
import pysc2.agents.Buildorder.buildorder as BuildOrder
import pysc2.agents.UnitDictionary.unitDictionary as Dictionary

# Other imports
import time
import queue
import numpy as np

# Necessary for console debugging of large arrays
np.set_printoptions(threshold=np.inf)

# Delay to slow down the game
DELAY_GAME = 0.05

# Fixed observation indices
IDX_SUPPLY_USED = 3
IDX_SUPPLY_MAX = 4
IDX_ARMY_COUNT = 8

WAR_MAX_STEPS = 1
ATTACK_ARMY_COUNT = 9
RESSOURCE_CHECK_INTERVAL = 4

# Cons
NO_OPERATION = actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

# BuildOrder request types
# Possible Types: Unit, Projectile, Destructible, Resource, Other, Structure, Shape, Item
class OBJECT_TYPE:
	UNIT = 'Unit'
	PROJECTILE = 'Projectile'
	DESTRUCTIBLE = 'Destructible'
	RESOURCE = 'Resource'
	OTHER = 'Other'
	STRUCTURE = 'Structure'
	SHAPE = 'Shape'
	ITEM = 'Item'
	UNDEFINED = None

# Class to provide states
class Statemachine:
    UNDEFINED = -1
    UPDATE_MODULES = 0
    BUILD_ORDER_REQUEST = 1
    CONSTRUCTION_BUILDER_RUNNING = 2
    UNIT_BUILDER_RUNNING = 3
    WAR_HANDLER_RUNNING = 4
    RESSOURCE_HANDLER_RUNNING = 5
    UPDATE_PROGRESS = 6

# Class to provide an agent which uses multiple modules to play SC2.
class SC2Agent(base_agent.BaseAgent):

    # Setup function is executed once on agents startup.
    def setup(self, obs_spec, action_spec):
        super(SC2Agent, self).setup(obs_spec, action_spec)

        # Create Queue to handle actions to execute
        self.actionQueue = queue.Queue()

        # Flag to enable or disable debug output
        self.debugOutputEnabled = False

        # Flag for executing something only once in step function
        self.executedOnce = False

        # TODO: Automatically detect race ?
        agentRace = 'Terr'

        # Create module class instances
        self.abstractionHub = AbstractionHub()
        self.unitBuilder = UnitBuilder(self.actionQueue, self.abstractionHub, debugOutputEnabled=False)
        self.consBuilder = ConstructionBuilder(self.actionQueue, self.abstractionHub, race=agentRace, debugOutputEnabled=False)
        self.ressourceHandler = Ressources(self.actionQueue, self.consBuilder,race=agentRace, debugOutputEnabled=False)
        self.warHandler = WarHandler(self.actionQueue, self.abstractionHub, debugOutputEnabled=False)

        # Other Attributes
        self.requestId = -1
        self.requestQuantity = 1
        self.requestType = OBJECT_TYPE.UNDEFINED
        self.possibleUpgradeId = None
        self.processedRequest = True
        self.startRequest = False
        self.consBuilderInterval = 10
        self.consBuilderIntervalCount = 0
        self.warStepsCounter = 0
        self.ressourceCheckCounter = 0

        self.currentState = Statemachine.UPDATE_MODULES
        self.previousState = Statemachine.UNDEFINED

    # Step function is executed each game step iteration.
    def step(self, obs):
        super(SC2Agent, self).step(obs)
        actionToExecute = NO_OPERATION

        # Delay to slow down the game
        time.sleep(DELAY_GAME)

        # Update obs in abstraction hub
        self.abstractionHub.getObservations(obs)

        # Process some initial operations depending on observations only once
        if not self.executedOnce:
            self.consBuilder.storeBasePosition(obs)
            #self.consBuilder.storeVespenGeyserPositions()
            self.executedOnce = True

        # Get next action from queue to process it
        if not self.actionQueue.empty():
            actionToExecute = self.getNextAction(obs)

        # Do updating stuff until new items appear in queue
        else:
            self.handleStates(obs)
            if not self.actionQueue.empty():
                actionToExecute = self.getNextAction(obs)

        # Return action to execute it
        return actionToExecute

    # Check if action is a valid action
    def actionIsValid(self, obs, actionToExecute):
        # Extract function id from FunctionCall
        fnc = str(actionToExecute).split('FunctionCall(function=')[1].split(', arguments')[0]

        if self.debugOutputEnabled:
            print("AGENT: action to execute =", fnc)
            print(obs.observation["available_actions"]);
        
        for f in np.nditer(obs.observation["available_actions"]):
            if fnc == str(f):
                return True
        
        print("AGENT: ACTION NOT VALID! ", str(actionToExecute))

        time.sleep(0.5)
        return False

    # Function to get next action from action queue
    def getNextAction(self, obs):
        nextAction = self.actionQueue.get()

        # Check if the choosen action is available
        if not self.actionIsValid(obs, nextAction):
            nextAction = NO_OPERATION
        return (nextAction)

    # Function to provide a statemachine
    def handleStates(self, obs):
        # State to do some fancy updating stuff
        if self.currentState == Statemachine.UPDATE_MODULES:
            self.updateModules(obs)

        # State to get new requests from build order
        elif self.currentState == Statemachine.BUILD_ORDER_REQUEST:
            self.buildOrderRequest(obs)

        # State to execute construction build process
        elif self.currentState == Statemachine.CONSTRUCTION_BUILDER_RUNNING:
            self.constructionBuilderRunning(obs)

        # State to execute unit build process
        elif self.currentState == Statemachine.UNIT_BUILDER_RUNNING:
            self.unitBuilderRunning(obs)

        # State to execute attack process
        elif self.currentState == Statemachine.WAR_HANDLER_RUNNING:
            self.warHandlerRunning(obs)

        # State to check available ressources and handle worker selection
        elif self.currentState == Statemachine.RESSOURCE_HANDLER_RUNNING:
            self.ressourceHandlerRunning(obs)

        # State to update progress of buildings and army
        elif self.currentState == Statemachine.UPDATE_PROGRESS:
            self.updateProgress(obs)

    def updateModules(self, obs):
        if self.debugOutputEnabled:
            print("STATEMACHINE: Entered UPDATE_MODULES")

        # Start attack if army limit is reached
        if obs.observation["player"][IDX_ARMY_COUNT] >= ATTACK_ARMY_COUNT and self.warStepsCounter < WAR_MAX_STEPS:
            self.currentState = Statemachine.WAR_HANDLER_RUNNING
            self.warStepsCounter = self.warStepsCounter + 1

        # Process build order otherwise
        else:
            if self.debugOutputEnabled:
                print("AGENT: Allow next build order step")
            self.currentState = Statemachine.BUILD_ORDER_REQUEST
            self.warStepsCounter = 0

        # Update construction builder to track building progress
        if self.consBuilderIntervalCount >= self.consBuilderInterval:
            self.consBuilder.setCurrentState(ConstructionBuilder.STATE_UPDATING)
            self.currentState = Statemachine.CONSTRUCTION_BUILDER_RUNNING
            self.consBuilderIntervalCount = 0
        else:
            if self.consBuilder.getCurrentState() == ConstructionBuilder.STATE_DONE:
                self.consBuilder.setCurrentState(ConstructionBuilder.STATE_READY)
                self.consBuilderIntervalCount += 1
            elif self.consBuilder.getCurrentState() == ConstructionBuilder.STATE_READY:
                self.consBuilderIntervalCount += 1

    def buildOrderRequest(self, obs):
        if self.debugOutputEnabled:
                print("STATEMACHINE: Entered BUILD_ORDER_REQUEST")

        # Get request from BuildOrder
        request = BuildOrder.getNextUnitId(obs.observation["player"][IDX_SUPPLY_USED], obs.observation["player"][IDX_SUPPLY_MAX])

        if request is not None:
            (self.requestId, self.possibleUpgradeId, self.requestQuantity) = request
            self.requestType = Dictionary.getObjectTypeById(self.requestId)

            # Change state
            if self.requestType == OBJECT_TYPE.STRUCTURE and self.possibleUpgradeId == None:
                self.ressourceHandler.setCurrentState(Ressources.STATE_CHECKING)
            else:
                self.ressourceHandler.setCurrentState(Ressources.STATE_CHECK_RESSOURCES)

            self.previousState = Statemachine.BUILD_ORDER_REQUEST
            self.currentState = Statemachine.RESSOURCE_HANDLER_RUNNING
        else:
            self.previousState = Statemachine.BUILD_ORDER_REQUEST
            self.currentState = Statemachine.UPDATE_MODULES

    def constructionBuilderRunning(self, obs):
        if self.debugOutputEnabled:
            print("STATEMACHINE: Entered CONSTRUCTION_BUILDER_RUNNING: %d" % (self.consBuilder.getCurrentState()))

        # Process building request if construction builder is ready
        if self.consBuilder.getCurrentState() == ConstructionBuilder.STATE_READY:
            if self.possibleUpgradeId == None:
                self.consBuilder.addNewBuilding(obs, self.requestId, baseIdx=0)
            else:
                self.consBuilder.upgradeBuilding(obs, self.requestId, self.possibleUpgradeId, baseIdx=0)

        elif self.consBuilder.getCurrentState() == ConstructionBuilder.STATE_BUILDING:
            self.consBuilder.startBuildProcess(obs)

        # Update buildings if construction builder is in state STATE_UPDATING
        elif self.consBuilder.getCurrentState() == ConstructionBuilder.STATE_UPDATING:
            self.consBuilder.updateBuildings(obs)

        # Change Agents state if construction builder is done
        elif self.consBuilder.getCurrentState() == ConstructionBuilder.STATE_DONE:
            if self.previousState == Statemachine.RESSOURCE_HANDLER_RUNNING:
                self.requestId = None
                         

            elif self.previousState == Statemachine.UPDATE_PROGRESS:
                self.consBuilder.setCurrentState(ConstructionBuilder.STATE_READY)
            self.ressourceHandler.setCurrentState(Ressources.STATE_SELECT_IDLE_WORKER)
            self.previousState = Statemachine.CONSTRUCTION_BUILDER_RUNNING
            self.currentState = Statemachine.RESSOURCE_HANDLER_RUNNING

    def unitBuilderRunning(self, obs):
        if self.debugOutputEnabled:
            print("STATEMACHINE: Entered UNIT_BUILDER_RUNNING")

        self.unitBuilder.setUnits(self.requestId, self.requestQuantity, obs, self.consBuilder)

        if self.unitBuilder.getCurrentState() == UnitBuilder.STATE_FINISHED:
            self.requestId = None
            self.previousState = Statemachine.UNIT_BUILDER_RUNNING
            self.currentState = Statemachine.UPDATE_MODULES
            self.unitBuilder.setCurrentState(UnitBuilder.STATE_SELECT_BUILDING)

    def warHandlerRunning(self, obs):
        if self.debugOutputEnabled:
            print("STATEMACHINE: Entered WAR_HANDLER_RUNNING")

        self.warHandler.attackEnemy(obs)
        self.previousState = Statemachine.WAR_HANDLER_RUNNING
        self.currentState = Statemachine.RESSOURCE_HANDLER_RUNNING

    def ressourceHandlerRunning(self, obs):
        if self.debugOutputEnabled:
            print("STATEMACHINE: Entered RESSOURCE_HANDLER_RUNNING")

        # selecting
        if self.ressourceHandler.getCurrentState() == Ressources.STATE_SELECTING:
            if self.ressourceHandler.fillUpVespeneMines:
                selected = self.ressourceHandler.selectWorker(obs, baseId=0, safeOne=True)
            else:
                selected = self.ressourceHandler.selectWorker(obs, baseId=0, safeOne=False)
            if selected:
                self.ressourceHandler.setCurrentState(Ressources.STATE_CHECKING)

        # checking
        elif self.ressourceHandler.getCurrentState() == Ressources.STATE_CHECKING:
            if self.ressourceHandler.isWorkerSelected(obs):
                if self.ressourceHandler.fillUpVespeneMines:
                    self.ressourceHandler.setCurrentState(Ressources.STATE_FILLING_UP)
                else:
                    self.ressourceHandler.setCurrentState(Ressources.STATE_CHECK_RESSOURCES)
                    self.previousState = Statemachine.RESSOURCE_HANDLER_RUNNING
                    self.currentState = Statemachine.CONSTRUCTION_BUILDER_RUNNING
            else:
                self.ressourceHandler.setCurrentState(Ressources.STATE_SELECTING)

        # select idle worker
        elif self.ressourceHandler.getCurrentState() == Ressources.STATE_SELECT_IDLE_WORKER:
            if self.ressourceHandler.hasIdleWorker(obs):
                self.ressourceHandler.selectIdleWorker()
                self.ressourceHandler.setCurrentState(Ressources.STATE_RETURN_IDLE_WORKER)
            else:
                self.ressourceHandler.setCurrentState(Ressources.STATE_FILLING_UP)

        # return idle worker
        elif self.ressourceHandler.getCurrentState() == Ressources.STATE_RETURN_IDLE_WORKER:
            if actions.FUNCTIONS.Harvest_Return_quick.id in obs.observation["available_actions"]:
                self.ressourceHandler.returnIdleWorkerQuick()
                self.ressourceHandler.setCurrentState(Ressources.STATE_FILLING_UP)
            else:
                returnedWorker = self.ressourceHandler.returnIdleWorker(obs)
                if returnedWorker:
                    self.ressourceHandler.setCurrentState(Ressources.STATE_FILLING_UP)

        # filling up vespene mines
        elif self.ressourceHandler.getCurrentState() == Ressources.STATE_FILLING_UP:
            filledUp = self.ressourceHandler.fillUpVespeneBuildings(obs, self.consBuilder.getVespeneMines())
            if filledUp:
                self.ressourceHandler.setCurrentState(Ressources.STATE_CHECK_RESSOURCES)
            else:
                self.ressourceHandler.setCurrentState(Ressources.STATE_SELECTING)

        # check ressources
        elif self.ressourceHandler.getCurrentState() == Ressources.STATE_CHECK_RESSOURCES:
            if self.requestId == None:
                self.currentState = Statemachine.UPDATE_MODULES
            else:
                if self.ressourceCheckCounter >= RESSOURCE_CHECK_INTERVAL:
                    self.currentState = Statemachine.UPDATE_PROGRESS
                    self.ressourceCheckCounter = 0

                elif self.ressourceHandler.checkIfPossible(obs, self.requestId):
                    if self.requestType == OBJECT_TYPE.UNIT:
                        self.currentState = Statemachine.UNIT_BUILDER_RUNNING
                    elif self.requestType == OBJECT_TYPE.STRUCTURE:
                        self.ressourceHandler.setCurrentState(Ressources.STATE_CHECKING)
                    self.ressourceCheckCounter = 0

                else:
                    self.ressourceCheckCounter = self.ressourceCheckCounter + 1

            self.previousState = Statemachine.RESSOURCE_HANDLER_RUNNING

    def updateProgress(self, obs):
        if self.debugOutputEnabled:
            print("STATEMACHINE: Entered UPDATE_PROGRESS")

        nextState = Statemachine.RESSOURCE_HANDLER_RUNNING

        # Start attack if army limit is reached
        if obs.observation["player"][IDX_ARMY_COUNT] >= ATTACK_ARMY_COUNT:
            nextState = Statemachine.WAR_HANDLER_RUNNING

        # Update construction builder to track building progress
        else:
            self.consBuilder.setCurrentState(ConstructionBuilder.STATE_UPDATING)
            nextState = Statemachine.CONSTRUCTION_BUILDER_RUNNING

        self.previousState = Statemachine.UPDATE_PROGRESS
        self.currentState = nextState
